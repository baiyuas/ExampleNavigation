# ExampleNavigation

#### Description

Android Navigation Component Example, This Repo show some basic usage, if you
want to learn more about navigation, please visit [https://developer.android.google.cn/guide/navigation](https://developer.android.google.cn/guide/navigation)

#### Software Architecture

Language: Kotlin
IDE: Android Studio Canary 4
JDK: 1.8
GradlePlugin: 3.6.0-alpha04
constraint-layout: 2.0.0-alpha3

#### Installation

1. $ git clone https://gitee.com/baiyuas/ExampleNavigation.git
2. Use Android Studio open this project

#### More

About the usage of Navigation, see the document [How To Use Navigation](doc/HowToUseNavigation.md)