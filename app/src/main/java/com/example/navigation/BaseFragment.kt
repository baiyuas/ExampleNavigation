package com.example.navigation

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
private const val TAG_LIFE = "LifeCycle---->"

open class BaseFragment : Fragment() {

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        log(TAG_LIFE, "onViewCreated")
        fragmentManager?.apply {
            log("Current fragmentManager back stack num: $backStackEntryCount")
        }

        log("Current childFragmentManager back stack num: $childFragmentManager.backStackEntryCount")
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        log(TAG_LIFE, "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log(TAG_LIFE, "onCreate")
    }

    override fun onStart() {
        super.onStart()
        log(TAG_LIFE, "onStart")
    }

    override fun onStop() {
        super.onStop()
        log(TAG_LIFE, "onStop")
    }

    override fun onPause() {
        super.onPause()
        log(TAG_LIFE, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(TAG_LIFE, "onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        log(TAG_LIFE, "onDestroyView")
    }

    override fun onDetach() {
        super.onDetach()
        log(TAG_LIFE, "onDetach")
    }
}