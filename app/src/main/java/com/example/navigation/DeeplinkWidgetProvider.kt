package com.example.navigation

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.os.Bundle
import android.widget.RemoteViews
import androidx.navigation.NavDeepLinkBuilder

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
class DeeplinkWidgetProvider : AppWidgetProvider() {


    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {

        appWidgetIds.forEach {
            appWidgetManager.updateAppWidget(
                it,
                RemoteViews(context.packageName, R.layout.deeplink_widget_layout).also { _it ->
                    _it.setOnClickPendingIntent(
                        R.id.btn_jump_deeplink,
                        NavDeepLinkBuilder(context)
                            .setGraph(R.navigation.nav_simple)
                            .setDestination(R.id.deeplinkFragment)
                            .setArguments(Bundle().apply {
                                putString("Arg_From", "AppWidget")
                            })
                            .createPendingIntent()
                    )
                }
            )
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }
}