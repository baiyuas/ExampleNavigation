package com.example.navigation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_first.*

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
class FirstFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // For Java, use Navigation. kotlin use extension function
        // btn_jump.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_firstFragment_to_secondFragment))

        btn_jump.setOnClickListener {
            findNavController().navigate(R.id.action_firstFragment_to_secondFragment)
        }

        btn_jump_safe_args2.setOnClickListener {
            findNavController().navigate(
                FirstFragmentDirections.actionFirstFragmentToReceiveArgsFragment(et_arg.text.toString().trim())
            )
        }

        btn_jump_safe_args.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToSecondFragment())
        }

        btn_jump_args.setOnClickListener {
            findNavController().navigate(
                R.id.action_firstFragment_to_receiveArgsFragment, bundleOf(
                    // about the `to` grammar see https://blog.csdn.net/android2me/article/details/73797255
                    "Arg_Name" to et_arg.text.trim().toString()
                )
            )
            // use core-ktx replace below code
            //  Bundle().apply {
            //      putString("Arg_Name", et_arg.text.trim().toString())
            //  }
        }

        btn_jump_deeplink.setOnClickListener {
            context?.let {
                findNavController().navigate(Uri.parse("http://www.deeplink.com/firstFragment"))
            }

        }

        btn_jump_anim.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToAnimFragment())
        }

        ViewCompat.setTransitionName(iv_thumb, SHARE_ELEMENT_NAME)
        iv_thumb.setOnClickListener {

            val extra = FragmentNavigatorExtras(
                iv_thumb to SHARE_ELEMENT_NAME
            )
            findNavController().navigate(
                FirstFragmentDirections.actionFirstFragmentToShareElementFragment(),
                extra
            )
        }

        btn_jump_drawer.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToDrawerNavActivity())
        }

        btn_jump_bottom_simple.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToSimpleBottomNavActivity())
        }

        btn_open_dialog.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToNavDialogFragment())
        }
    }
}