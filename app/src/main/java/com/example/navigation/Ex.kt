package com.example.navigation

import android.util.Log

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
fun log(msg: String) {
    Log.d("Log--->", "print msg: [$msg]")
}

fun log(tag:String, msg: String) {
    Log.d(tag, "print msg: [$msg]")
}