package com.example.navigation.bottom1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.navigation.R
import kotlinx.android.synthetic.main.activity_bottom_nav_sample.*

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
class SimpleBottomNavActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_nav_sample)
        val navController = findNavController(R.id.nav_host_container)
        bottom_nav_view.setupWithNavController(navController)
    }

}