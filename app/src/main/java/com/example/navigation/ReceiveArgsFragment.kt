package com.example.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_args.*

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */

class ReceiveArgsFragment : BaseFragment() {

    private val receiveArg by navArgs<ReceiveArgsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_args, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_show_arg.setOnClickListener {
            arguments?.let {
                tv_argument.text = it.getString("Arg_Name")
            }
        }

        btn_show_nav_args.setOnClickListener {
            tv_argument.text = receiveArg.ArgName
        }
    }

}