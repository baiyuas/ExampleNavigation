package com.example.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_second.*

/**
 * Copyright (c)2019
 * 拜雨 http://baiyuas.com
 *
 * @author lpc
 */
class SecondFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_back.setOnClickListener {
            findNavController().navigateUp()
        }

        btn_jump_anim.setOnClickListener {
            findNavController().navigate(SecondFragmentDirections.actionSecondFragmentToAnimFragment())
        }

        btn_jump_anim_no_pop.setOnClickListener {
            findNavController().navigate(SecondFragmentDirections.actionSecondFragmentToAnimFragmentNoPop())
        }
    }

}
